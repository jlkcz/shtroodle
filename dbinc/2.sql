BEGIN;

CREATE TABLE IF NOT EXISTS `sh_childrendata` (
  `id_childrendata` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_children` int(10) unsigned NOT NULL,
  `id_users` int(11) unsigned NOT NULL,
  `id_writeuptypes` int(11) unsigned NOT NULL,
  `content` text NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` TINYINT NOT NULL DEFAULT 0,
   PRIMARY KEY (`id_childrendata`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

CREATE TABLE IF NOT EXISTS `sh_writeuptypes` (
  `id_writeuptypes` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id_writeuptypes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

ALTER TABLE `sh_children`
  ADD `firstname` VARCHAR(100) DEFAULT NULL AFTER `active`,
  ADD `lastname` VARCHAR(100) DEFAULT NULL AFTER `firstname`,
  ADD `address` VARCHAR(255) DEFAULT NULL AFTER `lastname`,
  ADD `contact_phone` VARCHAR(64) DEFAULT NULL AFTER `address`,
  ADD `birthno` VARCHAR(64) DEFAULT NULL AFTER `contact_phone`,
  ADD `mother_name` VARCHAR(100) DEFAULT NULL AFTER `birthno`,
  ADD `mother_phone` VARCHAR(100) DEFAULT NULL AFTER `mother_name`,
  ADD `father_name` VARCHAR(100) DEFAULT NULL AFTER `mother_phone`,
  ADD `father_phone` VARCHAR(100) DEFAULT NULL AFTER `father_name`,
  ADD `date_member` VARCHAR(100) DEFAULT NULL AFTER `father_phone`,
  ADD `date_promise` VARCHAR(100) DEFAULT NULL AFTER `date_member`;



INSERT INTO `sh_writeuptypes` (name) VALUES ('Výchovné poznámky');
INSERT INTO `sh_writeuptypes` (name) VALUES ('Rodina');
INSERT INTO `sh_writeuptypes` (name) VALUES ('Oddíl');
INSERT INTO `sh_writeuptypes` (name) VALUES ('Ostatní');

INSERT INTO sh_dbversion (version) VALUES (2);

COMMIT;