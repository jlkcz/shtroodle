BEGIN;

DROP TABLE sh_dbversion;

DELETE FROM sh_config WHERE key IN ('bcc_to','smtp_on','smtp_user','smtp_pass','smtp_host','smtp_port');

DELETE FROM sh_dbversion WHERE version=1;

COMMIT;