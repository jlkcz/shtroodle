BEGIN;

CREATE TABLE IF NOT EXISTS `sh_turnout` (
  `id_turnout` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `poll` int(11) unsigned NOT NULL,
  `child` int(11) unsigned NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (`id_turnout`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;



INSERT INTO sh_dbversion (version) VALUES (7);

COMMIT;
