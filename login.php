<?php
require_once("lib/starter.php");
if(isset($_REQUEST["logout"])){
	setcookie('SHTROODLE','',time()-3600);
	session_destroy();
	my_header("login.php?msg=Úspěšně odhlášen");
}

	if(isset($_SESSION["auth"]) && $_SESSION["auth"]){
		my_header("index.php");
	}


if(isset($_POST["username"]) and isset($_POST["pass"])){
	$user_data = dibi::fetch("SELECT [id_users],[nick],[email],[passwd],[active],[superadmin] FROM [:sh:users] WHERE [login]=%s", $_POST["username"]);
	if($user_data["passwd"] != sha1($_POST["pass"])){
		my_header("login.php?err=Špatné uživatelské jméno nebo heslo","Incorrect username or password");
	}
	if(!$user_data["active"]){
		my_header("login.php?err=Váš účet je neaktivní! Aktivovat váš účet může administrátor.");
	}
	$_SESSION["auth"] = 1;
	$_SESSION["username"] = $_POST["username"];
	$_SESSION["nick"] = $user_data["nick"];
	$_SESSION["email"] = $user_data["email"];
	$_SESSION["userid"] = $user_data["id_users"];
	if($user_data["superadmin"] == 1){
		$_SESSION["superadmin"] = 1;
	}
	my_header("index.php?msg=Úspěšně přihlášen");

}


$template = $twig->loadTemplate("login.html");
$template->display(array("title" => "Přihlašte se"));




?>
