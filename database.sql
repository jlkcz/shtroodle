SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Databáze: `shtroodle`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `sh_children`
--
CREATE TABLE IF NOT EXISTS `sh_children` (
  `id_children` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nick` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_children`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------
--
-- Struktura tabulky `sh_equipment`
--
CREATE TABLE IF NOT EXISTS `sh_equipment` (
  `keyword` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  PRIMARY KEY (`keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `sh_groups`
--

CREATE TABLE IF NOT EXISTS `sh_groups` (
  `id_groups` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id_groups`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `sh_group_members`
--

CREATE TABLE IF NOT EXISTS `sh_group_members` (
  `group` int(11) unsigned NOT NULL DEFAULT '0',
  `child` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group`,`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `sh_polls`
--

CREATE TABLE IF NOT EXISTS `sh_polls` (
  `id_polls` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `place` varchar(200) NOT NULL,
  `date_start` date NOT NULL,
  `time_start` time NOT NULL,
  `place_start` varchar(200) NOT NULL,
  `date_end` date NOT NULL,
  `time_end` time NOT NULL,
  `place_end` varchar(200) NOT NULL,
  `equipment` tinytext,
  `price` int(10) DEFAULT NULL,
  `other_equipment` tinytext,
  `comment` text,
  `sent` datetime DEFAULT NULL,
  PRIMARY KEY (`id_polls`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `sh_plains`
--
CREATE TABLE IF NOT EXISTS `sh_plains` (
  `id_plains` int(10) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `content` text,
  `sent` datetime DEFAULT NULL,
  `file1` text,
  `file2` text,
  `file3` text
  PRIMARY KEY(`id_plains`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `sh_users`
--

CREATE TABLE IF NOT EXISTS `sh_users` (
  `id_users` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nick` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `login` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `passwd` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_czech_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `superadmin` tinyint(1) NOT NULL DEFAULT '0',
  `canapprove` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_users`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
-- passwd is SHA1()

INSERT INTO `sh_users` (`id_users`,`nick`,`login`,`passwd`,`email`,`active`,`superadmin`) VALUES (1,'test','Test',SHA1('test'),'test@domain.tld',1,1);
-- --------------------------------------------------------

--
-- Struktura tabulky `sh_votes`
--

CREATE TABLE IF NOT EXISTS `sh_votes` (
  `id_votes` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `poll` int(11) unsigned NOT NULL,
  `child` int(11) unsigned NOT NULL,
  `key` varchar(16) COLLATE utf8_czech_ci NOT NULL,
  `vote` tinyint(1) DEFAULT NULL,
  `comment` mediumtext COLLATE utf8_czech_ci,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_votes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;



CREATE TABLE IF NOT EXISTS `sh_config` (
  `key` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_czech_ci NOT NULL,
   PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `sh_config` (`key`, `value`) VALUES
('send_emails_from', 'from@domain.tld'),
('send_emails_reply_to', 'reply-to@domain.tld'),
('send_emails_subject', 'Pozvánka na výpravu: '),
('web_url', 'http://my-site/subdir/'),
('bcc_to', ''),
('smtp_on', ''),
('smtp_user', ''),
('smtp_pass', ''),
('smtp_host', ''),
('smtp_port', '');

